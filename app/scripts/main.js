document.addEventListener('DOMContentLoaded', () => {

  let burgerIcon = document.getElementsByClassName('header__burger')[0];
  let menu = document.getElementsByClassName('header__nav-box')[0];
  let intro = document.getElementsByClassName('intro__head')[0];

  burgerIcon.addEventListener('click', () => {
    if (menu.classList.contains('menu-expanded')) {
      menu.classList.remove('menu-expanded')

    } else {
      menu.classList.add('menu-expanded')
    }
    if (intro.classList.contains('intro__head--exp')) {
        intro.classList.remove('intro__head--exp')

    } else {
  intro.classList.add('intro__head--exp');
    }
  });

});
